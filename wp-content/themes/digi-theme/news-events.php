<?php get_header(); 
/**
* Template Name: News and Events
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php get_template_part("templates/hero"); ?>
<section class="news-events">
    <div class="container">
    <h1 class="h1 text-center ne-title"><?= get_the_title(); ?></h1>
    <?php $args = array('post_type' => 'news-events' );                                              
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {?>
        <?php
        while ( $the_query->have_posts() ) {
            $the_query->the_post(); ?>
            <div class="ne-post-item">
                <?php $button_label = get_field("ne_button");
                      $button_url = get_field("ne_button_url"); ?>
                <h2 class="post-title"><?= get_the_title(); ?></h2>
                <?= get_the_content(); ?>
                <?= get_the_post_thumbnail( get_the_ID(), 'full' ); ?>
                <a class="dark_button" href="<?= $button_url; ?>"><?= $button_label; ?></a>
            </div>
        <?php } ?>
        
    <?php /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
} ?>
    </div>
</section>
<?php get_footer(); ?>