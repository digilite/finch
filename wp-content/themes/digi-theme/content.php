<?php 
    if( have_rows("page_builder") ):

        // loop through the rows of data
        while ( have_rows("page_builder") ) : the_row();
            switch (get_row_layout()) :
                case "shop_by":
                    get_template_part("templates/page_builder/shop-by");
                    break;
                case "imgtr":
                    get_template_part("templates/page_builder/image-text-right");
                    break;
                case "cta":
                    get_template_part("templates/page_builder/cta");
                    break;
                case "product_info":
                    get_template_part("templates/page_builder/product-info");
                    break;
                case "contact_form":
                    get_template_part("templates/page_builder/contact-form");
                    break;
                case "text_info_block":
                    get_template_part("templates/page_builder/text-info");
                    break;
                case "services":
                    get_template_part("templates/page_builder/services");
                    break;
                case "accordion":
                    get_template_part("templates/page_builder/accordion");
                    break;
                case "stages":
                    get_template_part("templates/page_builder/stages");
                    break;
                case "contact":
                    get_template_part("templates/page_builder/contact");
                    break;
                case "map":
                    get_template_part("templates/page_builder/map");
                    break;
            endswitch;
        endwhile;
    else:
        the_content();
    endif;
?>