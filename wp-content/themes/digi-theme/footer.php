    </main>
    <footer itemscope itemtype="http://schema.org/WPFooter">
		    <div class="container">
		        <?php $logo = get_field('footer_logo', 'option'); ?>
		        <div class="flex-container footer-top justify-content-between">
                  <?php if( !empty( $logo ) ): ?>
                    <div class="footer-logo">
                      <img class="img-fluid" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" />
                    </div>
                  <?php endif; ?>
                  <div>
		                <?php wp_nav_menu([
                      'theme_location' => 'footer-menu-1',
                      'menu_class' => 'footer-menu footer-menu-1',
                      'container' => '',
                    ]); ?>
		            </div>
		            <div>
		                <?php wp_nav_menu([
                      'theme_location' => 'footer-menu-2',
                      'menu_class' => 'footer-menu footer-menu-2',
                      'container' => '',
                    ]); ?>
		            </div>
		            <div>
		                <?php wp_nav_menu([
							'theme_location' => 'footer-menu-3',
							'menu_class' => 'footer-menu footer-menu-3',
							'container' => '',
						]); ?>
		            </div>
		            <div class="footer-social flex-container justify-content-end">
		                <?php get_template_part("social"); ?>
		            </div>
		        </div>
		        <div class="flex-container footer-bottom justify-content-between">
		            <div class="copyright">
		                Copyright &copy; <?php echo date("Y") . " " . get_bloginfo("name"); ?> All Rights Reserved.
		            </div>
		            <div class="last-menu-footer">
		                <?php wp_nav_menu([
							'theme_location' => 'footer-menu-4',
							'menu_class' => 'footer-menu-4 flex-container justify-content-end',
							'container' => '',
						]); ?>
		            </div>
		        </div>
		    </div>
		</footer>
		<?php wp_footer(); ?>
		<div class="mobile-icons row justify-content-between">
		    <div class="search">
		        <img class="search-icon" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="search">
		        <p class="mob-title">Search</p>
		    </div>
		    <div class="account">
		        <a href="<?php echo get_site_url();?>/my-account" class="user-icon"><img
		                src="<?php bloginfo('template_directory'); ?>/img/user.svg" alt="user"></a>
		        <a class="mob-title" href="<?php echo get_site_url();?>/my-account">Account</a>
		    </div>
		    <div class="heart">
		        <?php echo do_shortcode('[ti_wishlist_products_counter]'); ?>
		        <a class="mob-title" href="<?php echo get_site_url();?>/wishlist">Wishlist</a>
		    </div>
		    <div class="cart">
		        <a href="<?php echo get_site_url();?>/cart" class="cart-icon"><img
		                src="<?php bloginfo('template_directory'); ?>/img/cart.svg" alt="cart"></a>
		        <p class="cart-contents">0</p>
		        <a class="mob-title" href="<?php echo get_site_url();?>/cart">Cart</a>
		    </div>
		</div>
		<div class="search-box search-box-mobile for-open">
		    <div class="container">
		        <div class="row justify-content-between">
		            <div class="search-field col-md-11">
		                <?php aws_get_search_form( true ); ?>
		            </div>
		        </div>
		    </div>
		</div>
  </body>

</html>
