<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>
<div class="row">
	<nav class="col-md-4 woocommerce-MyAccount-navigation">
		<ul>
			<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
				<?php if ( $endpoint != "downloads" ): //$endpoint != "dashboard" &&  ?>
					<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?> dashboard-menu-item">
						<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>" class="flex-container justify-content-between">
						   <?php echo esc_html( $label ); ?>
						   <svg width="32" height="31" viewBox="0 0 32 31" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M24.6973 11.2427L16.0002 19.6802L7.30304 11.2427" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                           </svg>
					    </a>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
	</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
