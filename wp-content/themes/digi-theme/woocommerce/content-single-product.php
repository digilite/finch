<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="row justify-content-between">
		<div class="col-md-6">
				<?php
				/**
				 * Hook: woocommerce_before_single_product_summary.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
				?>
				<?php require_once("single-product/tabs/tabs_new.php"); ?>
		</div>
		<div class="col-md-5 summary entry-summary">

			<?php
			global $product;
			$id = $product->id;
			$terms_brand = get_the_terms( $id, 'brand' );
				if($terms_brand):
					foreach ( $terms_brand as $term ) {
					$term_link = get_term_link( $term, 'brand' );
					if( is_wp_error( $term_link ) )
					continue;
					echo '<h4 class="pr-brand">' . $term->name . '</h4>';}
				endif;

			$terms_collection = get_the_terms( $id, 'collection' );
				if($terms_collection):
					foreach ( $terms_collection  as $term ) {
					$term_link = get_term_link( $term, 'collection' );
					if( is_wp_error( $term_link ) )
					continue;
					echo '<h4 class="pr-collcetion">' . $term->name . '</h4>';}
				endif;
			?>
	        <h3 class="pr-title"><?php the_title(); ?></h3>
			<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
				<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>
			<?php endif; ?>

			<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
			?>
			<?php require_once("single-product/tabs/tabs_new.php"); ?>
		</div>
	</div>
	<div class="related-block">
	       <?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
			?>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
