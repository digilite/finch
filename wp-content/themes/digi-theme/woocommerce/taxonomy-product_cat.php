
<?php get_header();
    $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
    $current_cat = $term->slug;
    if($current_cat == "watches") {
        $hero_image = get_field('watches_image', 'option');
        $brands_col = "col-md-4";
        $brands_desc = get_field('watches_description', 'option');
    } else if($current_cat == "accessories") {
        $hero_image = get_field('accessories_image', 'option');
        $brands_col = "col-md-12";
        $brands_desc = get_field('accessories_description', 'option');
    } else {
        $hero_image = get_field('jewellery_image', 'option');
        $brands_col = "col-md-4";
        $brands_desc = get_field('jewellery_description', 'option');
    }
    $size_static = 'finch-hero'; // (thumbnail, medium, large, full or custom size)

    $term = get_term_by('slug', $current_cat, 'product_cat');
    $current_cat_id = $term->term_id;
    $terms_cat = get_field('choose_brands', 'product_cat_'.$current_cat_id);?>
    <section class="hero">
            <?php $slide_image = get_field('category_hero_slider','product_cat_'.$current_cat_id); ?>

                <?php if( have_rows('category_hero_slider','product_cat_'.$current_cat_id) ): ?>
                <div class="hero-main-slider owl-carousel owl-theme" id="owl-demo">
                    <?php while( have_rows('category_hero_slider','product_cat_'.$current_cat_id) ) : the_row();
                        $slide_item = get_sub_field('cat_slide_item');
                        $size = 'finch-hero'; ?>
                        <div class="item">
                            <?php echo wp_get_attachment_image( $slide_item, $size ); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php else :
                if( !empty($hero_image) ): ?>
                    <?php echo wp_get_attachment_image( $hero_image, $size_static ); ?>
                <?php endif;
                endif; ?>
    <div class="logo-carousel">
        <div class="container">
            <div class="partners owl-carousel owl-theme">
            <?php  $terms = get_terms( 'brand', array(
                    'hide_empty' => 1
                ) );
                foreach ( $terms as $term ) {
                    $current_cat_id = $term->term_id;
                    $brand_image = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    if($brand_image):
                    $brand_thumb = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    else:
                    $brand_thumb = get_field('brands_default_image', 'option');
                    endif;
                    ?>
                    <a href="<?php echo get_site_url(); ?>/brand/<?php echo $term->slug; ?>" class="owl-item">
                        <div class="d-flex partner-l justify-content-center align-items-center">
                            <img class="partner-logo" src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>" />
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
    </section>

<div class="container">
    <div class="row justify-content-between brands-container">
        <div class="col-md-5 brands-block">
            <h2 class="h1"><?php echo ucfirst($current_cat); ?> By Brand</h2>
        </div>
        <div class="col-md-7 flex-container justify-content-start brands-block">
            <?php if($terms_cat) : ?>
            <div class="child-of-parent owl-carousel owl-theme">
                <?php
                if( $terms_cat ): ?>
                    <?php foreach( $terms_cat as $term ): ?>
                        <?php
                        $brand_image = get_field('brand_thumbnail', 'brand_'.$term->term_id);
                        if($brand_image) :
                        $brand_thumb = get_field('brand_thumbnail', 'brand_'.$term->term_id);
                        else: $brand_thumb = get_field('brands_default_image', 'option');
                        endif; ?>
                        <div class="item">
                            <a href="<?php echo get_site_url(); ?>/brand/<?php echo $term->slug; ?>"><img src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>"></a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?php else : ?>
            <div class="child-of-parent owl-carousel owl-theme">
                <?php
                $terms = get_terms( 'brand', array(
                    'hide_empty' => 1
                ) );
                foreach ( $terms as $term ) {
                $args = array(
                    'post_type' => 'product',
                    'product_cat' => $current_cat,
                    'brand' => $term->slug,
                    'posts_per_page' => 1
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) {
                while ( $query->have_posts() ) : $query->the_post();
                $tid = $term->term_id;
                $brand_image = get_field('brand_thumbnail', 'brand_'.$tid);
                if($brand_image) :
                $brand_thumb = get_field('brand_thumbnail', 'brand_'.$tid);
                else: $brand_thumb = get_field('brands_default_image', 'option');
                endif;
                ?>

                <div class="item">
                    <a href="<?php echo get_site_url(); ?>/shop/?yith_wcan=1&product_cat=<?php echo $current_cat; ?>&brand=<?php echo $term->slug; ?>"><img src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>"></a>
                    <a href="<?php echo get_site_url(); ?>/shop/?yith_wcan=1&product_cat=<?php echo $current_cat; ?>&brand=<?php echo $term->slug; ?>" class="brand-title text-center"><?= $term->name; ?></a>
                </div>
                <?php endwhile; }
                wp_reset_postdata(); } ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-5 brands-block">
            <h2 class="h1"><?php echo ucfirst($current_cat); ?></h2>
            <p><?= $brands_desc; ?></p>
        </div>
        <div class="col-md-7 flex-container justify-content-start brands-block">
            <?php
            $featured_posts = get_field('choose_product_to_show', 'product_cat_'.$current_cat_id);
            if( $featured_posts ): ?>
                <ul class="products flex-container">
                    <?php foreach( $featured_posts as $post ):
                        setup_postdata($post);
                        wc_get_template_part( 'content', 'product' ); ?>
                    <?php endforeach; ?>
                </ul>
                <?php
                wp_reset_postdata();
                else: ?>
                <ul class="products flex-container">
                    <?php
                    $args = array(
                        'post_type'   => 'product',
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'product_cat',
                                'field' => 'slug',
                                'terms' => $current_cat,
                            )
                        ),
                        'post_status' => 'publish',
                        'posts_per_page' => 9,  // -1 will get all the product. Specify positive integer value to get the number given number of product
                        'orderby'        => '_featured',
                        );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) : $the_query->the_post();

                            // Get default product template
                            wc_get_template_part( 'content', 'product' );

                        endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                    ?>
               </ul>
               <a class="see-all-brands-prod" href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?= $current_cat; ?>">See more products</a>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>

