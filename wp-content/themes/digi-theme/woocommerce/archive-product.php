<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>
<?php
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url_path = parse_url($url, PHP_URL_PATH);
	$basename = pathinfo($url_path, PATHINFO_BASENAME);
	$term = get_term_by('slug', $basename, 'brand');
	$tid = $term->term_id;
	$image = get_field('brand_banner', 'brand_' . $tid);
	if(!empty($image)) {
		$hero_image = get_field('brand_banner', 'brand_' . $tid);
	} else {
    	$hero_image = get_field('shop_page_hero', 'option');
	}
?>
<section class="breadcrumbs-container">
	<div class="container">
		<?php custom_breadcrumbs(); ?>
	</div>
</section>
<div class="container">
	<div class="row shop-products">
		<div class="col-md-3">
			<button class="pr-filter-button">REFINE BY</button>
			<div class="pr-filter">
				<?= do_shortcode('[br_filters_group group_id=1563]'); ?>
			</div>
		</div>
		<div class="col-md-9">
				<?php
				if ( woocommerce_product_loop() ) {

				/**
				 * Hook: woocommerce_before_shop_loop.
				 *
				 * @hooked woocommerce_output_all_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );

				woocommerce_product_loop_start();

				if ( wc_get_loop_prop( 'total' ) ) {
					while ( have_posts() ) {
						the_post();

						/**
						 * Hook: woocommerce_shop_loop.
						 */
						do_action( 'woocommerce_shop_loop' );

						wc_get_template_part( 'content', 'product' );
					}
				}

				woocommerce_product_loop_end();

				/**
				 * Hook: woocommerce_after_shop_loop.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			} else {
				/**
				 * Hook: woocommerce_no_products_found.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			} ?>
		</div>
	</div>
</div>
<?php
$cf_title = get_field("cf_title", 'option');
$cf_description = get_field("cf_description", 'option');
$cf_form_title = get_field("cf_form_title", 'option');
?>
<section class="contact-form">
    <div class="container">
        <div class="flex-container">
            <div class="col-md-5">
                <h3 class="h1"><?= $cf_title; ?></h3>
                <p class="description"><?= $cf_description; ?></p>
            </div>
            <div class="col-md-7">
                <p class="form-title"><?= $cf_form_title; ?></p>
                <?php echo do_shortcode('[formidable id=1]'); ?>
            </div>
        </div>
    </div>
</section>

<?php /**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */

get_footer( 'shop' );
