<?php
  get_header();
  $url = "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $url_path = parse_url($url, PHP_URL_PATH);
	$basename = pathinfo($url_path, PATHINFO_BASENAME);
	$term = get_term_by('slug', $basename, 'brand');
	$tid = 'brand_' . $term->term_id;
	$image = get_field('brand_banner', $tid);
  $size = 'finch-hero';
	if(!empty($image)):
		$hero_image = get_field('brand_banner', $tid);
  else:
    $hero_image = get_field('shop_page_hero', 'option');
	endif;
  $description = term_description();

?>
<section class="hero">
  <?php if( !empty($hero_image) ): ?>
  <?php echo wp_get_attachment_image( $hero_image, $size ); ?>
  <?php endif; ?>
</section>

<div class="container">
    <?php if($description): ?>
    <div class="brand_item_description col-md-10">
        <?= $description; ?>
    </div>
    <?php else: endif; ?>
    <div class="row justify-content-between brands-container">
      <?php
        $what_to_show = get_field('what_to_show_brand', $tid);
        if($what_to_show == "product_cat"):
            $t_title = "Category";
        else:
            $t_title = "Collection";
        endif;
        $collection =  get_field("what_to_show", $tid);
        $count_row = count(array($collection));
        if ( empty($count_row)):
      ?>
        <div class="col-md-5 brands-block">
            <h2 class="h1">Shop By <?= $t_title; ?></h2>
        </div>
        <div class="col-md-7 brands-block">
            <div class="<?php if($count_row > 3): echo 'child-of-parent owl-carousel owl-theme'; else: echo 'child-of-parent-brand flex-container not-slide'; endif; ?>">
                <?php
                  if( have_rows('what_to_show', $tid) ): while( have_rows('what_to_show', $tid) ) : the_row();
                    if($what_to_show == "product_cat"):
                        $termi = get_sub_field('rp_catgory_for_brand_page');
                    else:
                        $termi = get_sub_field('rp_collection_for_brand_page');
                    endif;
                    $thumb_id = get_woocommerce_term_meta( $termi->term_id, 'thumbnail_id', true );
                    $term_img = wp_get_attachment_url(  $thumb_id );
                    $imagei = get_sub_field('rp_br_image');
                    $sizei = 'cat-thumb';
                ?>
                <div class="item">
                    <a href="<?php echo get_site_url(); ?>/shop/products/product_cat/<?php echo $termi->slug; ?>/brand/<?= $basename; ?>"><?php if($imagei): echo wp_get_attachment_image( $imagei, $sizei ); else: echo '<img src="'.$term_img.'" alt="'.$term->name.'">'; endif;?></a>
                    <a href="<?php echo get_site_url(); ?>/shop/products/product_cat/<?php echo $termi->slug; ?>/brand/<?= $basename; ?>" class="brand-title text-center"><?php echo esc_html( $termi->name ); ?></a>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-md-5 brands-block">
          <?php $namebrand = $term->name; ?>
          <h2 class="h1 brand-name"><?= $namebrand; ?> Products</h2>
        </div>
        <div class="col-md-7 brands-block">
          <?php
            $featured_posts = get_field('brand_choose_prod', $tid);
            if( $featured_posts ):
          ?>
            <ul class="products flex-container">
              <?php
                foreach( $featured_posts as $post ): setup_postdata($post);
                wc_get_template_part( 'content', 'product' );
                endforeach;
              ?>
            </ul>
          <?php
            wp_reset_postdata();
            else:
          ?>
            <ul class="products flex-container">
              <?php
                $args = array(
                'post_type'   => 'product',
                'tax_query' => array(
                    array (
                        'taxonomy' => 'brand',
                        'field' => 'slug',
                        'terms' => $basename,
                    )
                ),
                'post_status' => 'publish',
                'posts_per_page' => 18,
                'orderby'  => 'modified',
                'order'    => 'DESC'
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                    // Get default product template
                    wc_get_template_part( 'content', 'product' );
                endwhile;
                } else {
                    echo __( 'No products found' );
                }
                wp_reset_postdata();
            ?>
            </ul>
            <a class="see-all-brands-prod" href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/brand/<?= $basename; ?>">See more products</a>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
