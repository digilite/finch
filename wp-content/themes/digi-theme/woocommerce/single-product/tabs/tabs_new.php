<?php

$movement = get_field("product_movement");
$features = get_field("product_features"); ?>

            <div class="clear: both;"></div>
			<div class="b-faq">
                <div class="faq__item">
                        <div class="faq__title js-faq-title">
                            <div class="faq__spoiler js-faq-rotate"><span class="faq__symbol "><img src="<?php bloginfo('template_directory'); ?>/img/arr.svg" alt="arr"></span></div> 
                            <span class="faq__text h3">Description</span>
                        </div>
                        <div class="faq__content js-faq-content">
                            <?php the_content(); ?>
                        </div>
						<?php if($movement): ?>
						<div class="faq__title js-faq-title">
                            <div class="faq__spoiler js-faq-rotate"><span class="faq__symbol "><img src="<?php bloginfo('template_directory'); ?>/img/arr.svg" alt="arr"></span></div> 
                            <span class="faq__text h3">Movement</span>
                        </div>
                        <div class="faq__content js-faq-content">
                            <?= $movement; ?>
                        </div>
						<?php endif; ?>
						<?php if($features): ?>
						<div class="faq__title js-faq-title">
                            <div class="faq__spoiler js-faq-rotate"><span class="faq__symbol "><img src="<?php bloginfo('template_directory'); ?>/img/arr.svg" alt="arr"></span></div> 
                            <span class="faq__text h3">Features</span>
                        </div>
                        <div class="faq__content js-faq-content">
                            <?= $features; ?>
                        </div>
						<?php endif; ?>
				</div>
			</div>			

		<?php // do_action( 'woocommerce_product_after_tabs' ); ?>
	


