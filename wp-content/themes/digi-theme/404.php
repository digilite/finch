<?php get_header(); ?>

<div class="container page-404 text-center">
	<h1 class="err_title">404</h1>
	<h3 class="err_subtitle">Error</h3>
	<p class="err_desc">Page Not Found</p>
	<a class="err_back" href="<?php bloginfo('url'); ?>">
		Back to Homepage
	</a>
</div>

<?php get_footer(); ?>