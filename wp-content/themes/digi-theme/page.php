<?php get_header(); ?>

<?php
    if (is_page("about-us") || is_page("contact-us") || is_page("customer-care")) {
         get_template_part("templates/hero"); 
         get_template_part("content"); 
    } else {
?>
<main class="main-content user-content">
    <div class="container">
        
        <?php the_content(); ?>
    </div>
</main>
<?php } ?>
<?php get_footer(); ?>