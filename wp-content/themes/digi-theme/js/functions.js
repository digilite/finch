var $ = jQuery;
var single_slide_bullets_fade = {
	loop: true,
	dots: false,
    autoplay: true,
    nav:true,
    lazyLoad: true,
    margin:24,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M216.4 163.7c5.1 5 5.1 13.3.1 18.4L155.8 243h231.3c7.1 0 12.9 5.8 12.9 13s-5.8 13-12.9 13H155.8l60.8 60.9c5 5.1 4.9 13.3-.1 18.4-5.1 5-13.2 5-18.3-.1l-82.4-83c-1.1-1.2-2-2.5-2.7-4.1-.7-1.6-1-3.3-1-5 0-3.4 1.3-6.6 3.7-9.1l82.4-83c4.9-5.2 13.1-5.3 18.2-.3z" /><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)" /></svg>Previous','Next<svg viewBox="0 0 512 512"><path d="M322.2 349.7c-3.1-3.1-3-8 0-11.3l66.4-74.4H104c-4.4 0-8-3.6-8-8s3.6-8 8-8h284.6l-66.3-74.4c-2.9-3.4-3.2-8.1-.1-11.2 3.1-3.1 8.5-3.3 11.4-.1 0 0 79.2 87 80 88s2.4 2.8 2.4 5.7-1.6 4.9-2.4 5.7-80 88-80 88c-1.5 1.5-3.6 2.3-5.7 2.3s-4.1-.8-5.7-2.3z" /><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)" /></svg>'],
	autoplayTimeout: 7000,
	responsiveClass: true,
    responsive:{
        0:{
            items:1,
            autoHeight:true,
        },
        992:{
            items:2,
        },
    }
};

var single_slide_bullets = {
	loop: true,
    autoplay: true,
    animateIn: "fadeIn",
	animateOut: "fadeOut", 
	autoplayTimeout: 7000,
    items:1,
	responsiveClass: true,
    responsive:{
        0:{
            dots: false,
            autoHeight:true,
        },
        768:{
            dots: true,
        },
    }
};


var slider_section = $('.slider-section');
var testimonials_slider = $(".testimonials-slider");
var service_slider = $(".service-slider");

$(document).ready(function() {

    

      $('.show-categories').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
    responsive: {

        0: {
            items: 2,
            loop: true
        },

        560: {
            items: 2,
            loop: true
        },

        991: {
            items: 3,
            loop: false
        },

    },
    })


    function getItems(items) {
        if (myItemCount < items) {
            return myItemCount;
        }
        else {
            return items;
        }
    }


    function getLoop(items) {
        if (myItemCount < items) {
            return false;
        }
        else {
            return true;
        }
    }









//BEGIN
$(".js-faq-title").on("click", function(e) {

    e.preventDefault();
    var $this = $(this);

    if (!$this.hasClass("faq__active")) {
        $(".js-faq-content").slideUp(800);
        $(".js-faq-title").removeClass("faq__active");
        $('.js-faq-rotate').removeClass('faq__rotate');
    }

    $this.toggleClass("faq__active");
    $this.next().slideToggle();
    $('.js-faq-rotate',this).toggleClass('faq__rotate');
});
//END



    $('.pr-filter-button').click(function(){
        $('.pr-filter').slideToggle();
    })

    $("#burger-icon").click(function(){
        $(this).toggleClass("open");
        $(".mobile-menu").toggleClass("visible");
    });

    if(slider_section.length) {
		slider_section.owlCarousel(single_slide_bullets);
		slider_section.find(".owl-controls .owl-dots").wrap("<div class='container'></div>")
	}

	if(testimonials_slider.length) {
		testimonials_slider.owlCarousel(single_slide_bullets_fade);
	}
	if(service_slider.length) {
		service_slider.owlCarousel(full_gutter_24);
    }
    $(".user-content iframe").each(function(){
        $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>")
    });
    mobileMenu();
    
    $(".move-down").click(function(){
        var hero_height = $(this).parent().parent().height();
        var to_scroll = hero_height + $("header").height() + 34;
        $("html, body").animate({ scrollTop:  to_scroll }, 2000);
    });
    if ($(".grid").length) {
        $(".grid").packery({
            // options
            itemSelector: ".grid-item",
        });
    }


    $(".search-icon").click(function(){
        $(".for-open").removeClass("opened");
        $(".search-box").addClass("opened");
    })

    $(".accessories").hover(function(){
        $(".for-open").removeClass("opened");
        $(".brands-megamenu-accessories").addClass("opened");
    })

    $('.has-sub').click(function(){
        $('.mobile-menu').addClass('mobile-menu-slide');
    })
    
    $('.back-to-main').click(function(){
        $('.mobile-menu').removeClass('mobile-menu-slide');
        $('.brands-megamenu').removeClass("opened");
    })

    $(".brands-megamenu-accessories").mouseover(function(){
        $(this).addClass("opened");
    })

    $(".accessories").mouseout(function(){
        $(".brands-megamenu-accessories").removeClass("opened");
    })

    $(".brands-megamenu-accessories").mouseout(function(){
        $(this).removeClass("opened");
    })


    $(".watches").hover(function(){
        $(".for-open").removeClass("opened");
        $(".brands-megamenu-watches").addClass("opened");
    })

    $(".watches").mouseout(function(){
        $(".brands-megamenu-watches").removeClass("opened");
    })

    $(".brands-megamenu-watches").mouseover(function(){
        $(this).addClass("opened");
    })

    $(".brands-megamenu-watches").mouseout(function(){
        $(this).removeClass("opened");
    })



    $(".jewellery").hover(function(){
        $(".for-open").removeClass("opened");
        $(".brands-megamenu-jewellery").addClass("opened");
    })

    $(".jewellery").mouseout(function(){
        $(".brands-megamenu-jewellery").removeClass("opened");
    })

    $(".brands-megamenu-jewellery").mouseover(function(){
        $(this).addClass("opened");
    })

    $(".brands-megamenu-jewellery").mouseout(function(){
        $(this).removeClass("opened");
    })



    $(".brands_menu_item").click(function(){
        $(".brands_menu").addClass("opened_brands");
    })

   /* $(".close").click(function(){
        $(".for-open").removeClass("opened");
    }) */


    $(document).mouseup(function(e){
        var container = $(".search-box");
     
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0){
            container.removeClass('opened');
        }
    });



    $('.partners').owlCarousel({
        loop:true,
        margin:20,
        responsiveClass:true,
        nav:true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:true
            },
            1000:{
                items:5,
                nav:true,
                loop:true
            }
        }
    })


    $('.child-of-parent').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0: {
                items: 1,
                loop: true
            },
    
            560: {
                items: 2,
                loop: true
            },
    
            991: {
                items: 3,
                loop: true
            },
        }
    })

    $('#billing_phone').mask('(000)000-0000');
    $('#billing_postcode').mask('A0A 0A0');

    $('.brands-menu-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0: {
                items: 2,
                loop: true
            },
    
            560: {
                items: 2,
                loop: true
            },
    
            991: {
                items: 4,
                loop: true
            },
        }
    })


    $("#owl-demo").owlCarousel({
        nav : true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        dots: true,
        loop: true,
        autoplay: true,
        controls: true,
        slideSpeed : 300,
        paginationSpeed : 400,
        items : 1, 
        itemsDesktop : false,
        itemsDesktopSmall : false,
        itemsTablet: false,
        itemsMobile : false
    
    });



    $('#load-more-brands').on('click', function(e){
        e.preventDefault();
        var offset = $(document).find(".brand-footer .brands-item-footer").length;        
        console.log(offset);
        $.ajax({
            method: 'POST',
            url: wp_var.ajax_url,
            type: 'JSON',
            data: {
                action: 'load_more_posts',
                offset: offset
            },
            success:function(response){
                var $data = $(response);
                if($data.length){
                    $(".brand-footer").append($data);
                    $("#load-more-brands").attr("disabled",false);
                } else{
                    $("#load-more-brands").css("cursor","none");
                    $("#load-more-brands").text("No brands found");
                }                              
            }
        }); 
    })




    /*$('.child-of-parent').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        nav:true,
        navText: ['<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.625 23.25L1.375 12L12.625 0.75" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.375 0.75L12.625 12L1.375 23.25" stroke="#555555" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:3,
                nav:true,
                loop:true
            }
        }
    })*/

    



});

function mobileMenu() {
    if ($(window).width() < 992 ) {
        $(".menu-item-has-children").click(function(e){
            $('.sub-menu').css('display','none');
            $(this).find(".sub-menu").css('display','flex');
        });
    }
}