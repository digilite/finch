<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("template_url") ?>/img/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("template_url") ?>/img/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_url") ?>/img/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("template_url") ?>/img/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_url") ?>/img/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("template_url") ?>/img/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("template_url") ?>/img/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("template_url") ?>/img/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_url") ?>/img/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("template_url") ?>/img/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("template_url") ?>/img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("template_url") ?>/img/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("template_url") ?>/img/favicon-16x16.png">
  <link rel="manifest" href="<?php bloginfo("template_url") ?>/img/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php bloginfo("template_url") ?>/img/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="mobile-menu">
		<div class="container">
			<div class="mobile-nav">
				<?php wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'main-menu',
					'container' => '',
				]); ?>
			</div>
		</div>
	</div>
	<header itemscope itemtype="http://schema.org/WPHeader">
		<div class="header-top">
			<div class="container">
				<div class="row relative-parent mobile-header">
					<div class="phone-number col-md-4 flex-container">
					<svg class="phone-icon" width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M20.1402 16.5307C19.3958 15.7807 17.593 14.6862 16.7183 14.2451C15.5792 13.6713 15.4855 13.6245 14.5902 14.2896C13.993 14.7335 13.5959 15.1301 12.897 14.981C12.1981 14.832 10.6794 13.9915 9.34954 12.6659C8.01969 11.3402 7.13047 9.77742 6.98094 9.08086C6.83141 8.38429 7.23454 7.99195 7.67422 7.39336C8.29391 6.54961 8.24704 6.40898 7.71735 5.26992C7.30438 4.38398 6.17797 2.59805 5.42516 1.85742C4.61985 1.06195 4.61985 1.20258 4.10094 1.4182C3.67849 1.59595 3.27321 1.812 2.89016 2.06367C2.14016 2.56195 1.72391 2.97586 1.43282 3.59789C1.14172 4.21992 1.01094 5.6782 2.51422 8.40914C4.01751 11.1401 5.07219 12.5365 7.25516 14.7134C9.43813 16.8902 11.1167 18.0607 13.5706 19.437C16.6063 21.1371 17.7706 20.8057 18.3945 20.5151C19.0184 20.2245 19.4342 19.812 19.9334 19.062C20.1858 18.6796 20.4023 18.2747 20.5803 17.8526C20.7964 17.3355 20.937 17.3355 20.1402 16.5307Z" stroke="#111111" stroke-width="1.5" stroke-miterlimit="10"/>
					</svg>
					    <?php $main_phone = get_field('main_phone_number', 'option');
						$clear_phone = str_replace(' ', '', $main_phone);
						$part_phone = str_replace(')', '', $clear_phone);
						$part2_phone = str_replace('(', '', $part_phone);
						$final_phone = str_replace('-', '', $part2_phone);
						?>

						<a href="tel:<?= $final_phone; ?>" class="phone-link"><?= $main_phone; ?></a>
						<div id="burger-icon">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="search-icon-box search-mobile">
							<img class="search-icon" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="search">
						</div>
					</div>
					<div itemscope itemtype="http://schema.org/Organization" id="logo" class="col-md-4 textcenter">
						<a itemprop="url" href="<?php echo bloginfo('url') ?>">
							<img class="img-fluid" itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg">
						</a>
					</div>
					<div class="icons col-md-4 flex-container">
						<div class="">
						<a href="<?php echo get_permalink( get_page_by_path( 'my-account' ) ); ?>" class="user-icon"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M16.1249 6.75C15.9412 9.22828 14.0624 11.25 11.9999 11.25C9.93745 11.25 8.05542 9.22875 7.87495 6.75C7.68745 4.17188 9.51557 2.25 11.9999 2.25C14.4843 2.25 16.3124 4.21875 16.1249 6.75Z" stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
							<path d="M12 14.25C7.92187 14.25 3.7828 16.5 3.01687 20.7469C2.92452 21.2588 3.21421 21.75 3.74999 21.75H20.25C20.7862 21.75 21.0759 21.2588 20.9836 20.7469C20.2172 16.5 16.0781 14.25 12 14.25Z" stroke="#111111" stroke-width="1.5" stroke-miterlimit="10"/>
							</svg>
						</a>
						</div>
						<div class="">
						<?php echo do_shortcode('[ti_wishlist_products_counter]'); ?>
						</div>
						<div class="cart-ic">
							<a href="#" class="cart-icon"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M3.75 8.25C3.55109 8.25 3.36032 8.32902 3.21967 8.46967C3.07902 8.61032 3 8.80109 3 9V19.125C3 20.5425 4.2075 21.75 5.625 21.75H18.375C19.7925 21.75 21 20.6011 21 19.1836V9C21 8.80109 20.921 8.61032 20.7803 8.46967C20.6397 8.32902 20.4489 8.25 20.25 8.25H3.75Z" stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M7.5 8.25V6.75C7.5 5.55653 7.97411 4.41193 8.81802 3.56802C9.66193 2.72411 10.8065 2.25 12 2.25V2.25C13.1935 2.25 14.3381 2.72411 15.182 3.56802C16.0259 4.41193 16.5 5.55653 16.5 6.75V8.25" stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M7.5 10.5V11.25C7.5 12.4435 7.97411 13.5881 8.81802 14.432C9.66193 15.2759 10.8065 15.75 12 15.75C13.1935 15.75 14.3381 15.2759 15.182 14.432C16.0259 13.5881 16.5 12.4435 16.5 11.25V10.5" stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</a>
							<p class="cart-contents">0</p>
							<div class="mini">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="navigation">
			<div class="container">
				<div class="flex-container nav-menu">
					<nav itemscope itemtype="http://schema.org/SiteNavigationElement" class="main-nav">
						<?php wp_nav_menu([
						'theme_location' => 'primary-menu',
						'menu_class' => 'main-menu',
						'container' => '',
						]); ?>
					</nav>
					<div class="search-icon-box">
						<svg class="search-icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M10.3635 3C8.90709 3 7.48342 3.43187 6.27248 4.24099C5.06154 5.05011 4.11773 6.20015 3.5604 7.54567C3.00307 8.89119 2.85724 10.3718 3.14137 11.8002C3.4255 13.2286 4.12681 14.5406 5.15663 15.5704C6.18645 16.6003 7.49851 17.3016 8.92691 17.5857C10.3553 17.8698 11.8359 17.724 13.1814 17.1667C14.5269 16.6093 15.677 15.6655 16.4861 14.4546C17.2952 13.2437 17.7271 11.82 17.7271 10.3636C17.7269 8.41069 16.9511 6.5378 15.5702 5.15688C14.1893 3.77597 12.3164 3.00012 10.3635 3V3Z" stroke="#111111" stroke-width="1.5" stroke-miterlimit="10"/>
							<path d="M15.8573 15.8574L21 21.0001" stroke="#111111" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round"/>
						</svg>
					</div>
				</div>
			</div>
		</div>
		<div class="search-box for-open">
				<div class="container">
					<div class="row justify-content-center">
						<div class="search-field col-md-10">
						<?php aws_get_search_form( true ); ?>
          </div>
				</div>
			</div>
		</div>
		<div class="brands-megamenu-watches brands-megamenu for-open">
			<div class="container">
				<div class="wb-row">
					<?php
					if( have_rows('watches_brands', 'option') ):
						while( have_rows('watches_brands', 'option') ) : the_row(); ?>
							<div class="item-col col-md-3">
								<?php $term_brand = get_sub_field('w_choose_brand');
								if( $term_brand ): ?>
									<a class="wb-brand" href="<?php echo get_site_url(); ?>/brand/<?php echo esc_html( $term_brand->slug ); ?>"><?php echo esc_html( $term_brand->name ); ?></a>
								<?php endif;
								if( have_rows('w_choose_attributes', 'option') ):
									while( have_rows('w_choose_attributes', 'option') ) : the_row();
										$term_attr = get_sub_field('w_add_attribute');
										$term_object = get_term( $term_attr );
										if( $term_attr ): ?>
											<a class="wb-attr" href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/brand/<?php echo esc_html( $term_brand->slug ); ?>/<?php echo esc_html( $term_object->taxonomy ); ?>/<?php echo esc_html( $term_object->slug ); ?>"><?= $term_object->name; ?></a>
										<?php endif;
									endwhile;
								else :
								endif; ?>
							</div>
						<?php endwhile;
					else :
					endif; ?>
				</div>
				<p class="back-to-main">Back to main menu</p>
			</div>
		</div>

		<div class="brands-megamenu-accessories brands-megamenu for-open">
			<div class="container">
				<div class="wb-row">
					<?php
					if( have_rows('acc_brands', 'option') ):
						while( have_rows('acc_brands', 'option') ) : the_row(); ?>
							<div class="item-col col-md-3">
								<?php $term_brand = get_sub_field('acc_choose_brand');

								if( $term_brand ): ?>
									<a class="wb-brand" href="<?php echo get_site_url(); ?>/brand/<?php echo esc_html( $term_brand->slug ); ?>"><?php echo esc_html( $term_brand->name ); ?></a>
								<?php endif;
								if( have_rows('choose_acc_attribute', 'option') ):
									while( have_rows('choose_acc_attribute', 'option') ) : the_row();
										$term_attr = get_sub_field('acc_add_attribute');
										$term_object = get_term( $term_attr );
										if( $term_attr ): ?>
											<a class="wb-attr" href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/brand/<?php echo esc_html( $term_brand->slug ); ?>/<?php echo esc_html( $term_object->taxonomy ); ?>/<?php echo esc_html( $term_object->slug ); ?>"><?= $term_object->name; ?></a>
										<?php endif;
									endwhile;
								else :
								endif; ?>
							</div>
						<?php endwhile;
					else :
					endif; ?>
				</div>
				<p class="back-to-main">Back to main menu</p>
			</div>
		</div>

		<div class="brands-megamenu-jewellery brands-megamenu for-open">
			<div class="container">
				<div class="wb-row">
					<?php
					if( have_rows('jewellery_brands', 'option') ):
						while( have_rows('jewellery_brands', 'option') ) : the_row(); ?>
							<div class="item-col col-md-3">
								<?php $term_brand = get_sub_field('jew_choose_brand');
								if( $term_brand ): ?>
									<a class="wb-brand" href="<?php echo get_site_url(); ?>/brand/<?php echo esc_html( $term_brand->slug ); ?>"><?php echo esc_html( $term_brand->name ); ?></a>
								<?php endif;
								if( have_rows('jew_choose_attribute', 'option') ):
									while( have_rows('jew_choose_attribute', 'option') ) : the_row();
										$term_attr = get_sub_field('jew_add_attribte');
										$term_object = get_term( $term_attr );
										if( $term_attr ): ?>
											<a class="wb-attr" href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>brand/<?php echo esc_html( $term_brand->slug ); ?>/products/<?php echo esc_html( $term_object->taxonomy ); ?>/<?php echo esc_html( $term_object->slug ); ?>"><?= $term_object->name; ?></a>
										<?php endif;
									endwhile;
								else :
								endif; ?>
							</div>
						<?php endwhile;
					else :
					endif; ?>
				</div>
				<p class="back-to-main">Back to main menu</p>
			</div>
		</div>
		<div class="brands_menu">
		   <div class="container">
			   <div class="owl-carousel owl-theme brands-menu-carousel">
					<?php $terms = get_terms( 'brand', array(
						'hide_empty' => 1
					) );
					foreach ( $terms as $term ) {
					$brand_image = get_field('brand_thumbnail', 'brand_'.$tid);
					if($brand_image) :
					$brand_thumb = get_field('brand_thumbnail', 'brand_'.$tid);
					else: $brand_thumb = get_field('brands_default_image', 'option');
					endif;?>
				    <div class="text-left">
						<a href="#"><img src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>"></a>
						<a href="#" class="brand-title text-center"><?= $term->name; ?></a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</header>
  <main>
