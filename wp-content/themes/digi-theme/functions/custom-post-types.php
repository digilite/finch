<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_events() {
	$labels = [
		"name"               => _x("News and Events", "news-events"),
		"singular_name"      => _x("News and Events", "news-events"),
		"add_new"            => _x("Add New", "Event or News"),
		"add_new_item"       => __("Add New"),
		"edit_item"          => __("Edit"),
		"new_item"           => __("New"),
		"all_items"          => __("All"),
		"view_item"          => __("View"),
		"search_items"       => __("Search"),
		"not_found"          => __("No items found"),
		"not_found_in_trash" => __("No items found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "News and Events"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
	];
	register_post_type("news-events", $args);
}
add_action("init", "custom_post_type_events");
