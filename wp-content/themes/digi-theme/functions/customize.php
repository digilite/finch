<?php
/* WYSIWYG defaults */
/** change tinymce's paste-as-text functionality */
function paste_as_text($mceInit, $editor_id) {
	//turn on paste_as_text by default
	//NB this has no effect on the browser's right-click context menu's paste!
	$mceInit["paste_as_text"] = true;
	return $mceInit;
}
add_filter("tiny_mce_before_init", "paste_as_text", 1, 2);

/** Set the Attachment Display Settings, This function is attached to the 'after_setup_theme' action hook. */
function default_attachment_display_setting() {
	update_option("image_default_align", "left");
	update_option("image_default_link_type", "none");
	update_option("image_default_size", "large");
}
add_action("after_setup_theme", "default_attachment_display_setting");

// CUSTOM MENUS
function custom_menus() {
	register_nav_menus(
		[
			"primary-menu" => __("Primary Menu"),
			"secondary-menu" => __("Secondary Menu"),
			"footer-menu-1" => __("Footer Menu 1"),
			"footer-menu-2" => __("Footer Menu 2"),
			"footer-menu-3" => __("Footer Menu 3"),
			"footer-menu-4" => __("Footer Menu Bottom"),
		]
	);
}
add_action("init", "custom_menus");



// Add the custom columns to the Services post type:
// add_filter( 'manage_cpt_services_posts_columns', 'set_custom_edit_cpt_services_columns' );
// function set_custom_edit_cpt_services_columns($columns) {
//     $columns['featured'] = __( 'Featured', 'your_text_domain' );
//     return $columns;
// }

// Add the data to the custom columns for the book post type:
// add_action( 'manage_cpt_services_posts_custom_column' , 'custom_cpt_services_column', 10, 2 );
// function custom_cpt_services_column( $column, $post_id ) {
// 	$column_field = 'make_this_service_featured';
//     switch ( $column ) {
// 		case 'featured' : 
// 			$post_meta = get_field($column_field,$post_id);
// 			if ($post_meta) {
// 				echo "Yes";
// 			}else {
// 				echo "No";
// 			}
// 		break;

//     }
// }


// Get Formidable Forms in a ACF filed
// function acf_load_color_field_choices( $field ) {
// 	global $wpdb;
// 	$forms = $wpdb->get_results('SELECT * FROM wp_frm_forms WHERE status="published"');
// 	$ids = array();
// 	$values = array();
// 	$i=0;
// 	if ( $forms != NULL ) {
// 		foreach($forms as $form){
// 			$ids[$i] = $form->id;
// 			$values[$i] = $form->name;
// 			$i++;
// 		}
// 		$form_assoc = array_combine($ids, $values);
// 		if( is_array($form_assoc) ){
// 			foreach( $form_assoc as $key=>$match ){
// 					$field["choices"][ $key ] = $match;
// 			}
// 		}
// 		// return the field
// 		return $field;
// 	} else {
// 		return false;
// 	}
    
// }

// Enter the fild key below
// add_filter("acf/load_field/key=field_5ce6e312e7c38", "acf_load_color_field_choices");



// create an ID from a user entered string and removing any unwanted symbols
function create_id($string) {
	$new_id = preg_replace('/[^a-zA-Z]/', '', $string);
	$new_id = strtolower(str_replace(" ", "", $new_id));
	return $new_id;
}

function post_back_link() {
	if (wp_get_referer()) {
		$prev_url = $_SERVER['HTTP_REFERER'];
		return "<a href='". $prev_url ."' class='back-link'><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' aria-hidden='true' focusable='false' style='-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);' preserveAspectRatio='xMidYMid meet' viewBox='0 0 512 512'><path d='M216.4 163.7c5.1 5 5.1 13.3.1 18.4L155.8 243h231.3c7.1 0 12.9 5.8 12.9 13s-5.8 13-12.9 13H155.8l60.8 60.9c5 5.1 4.9 13.3-.1 18.4-5.1 5-13.2 5-18.3-.1l-82.4-83c-1.1-1.2-2-2.5-2.7-4.1-.7-1.6-1-3.3-1-5 0-3.4 1.3-6.6 3.7-9.1l82.4-83c4.9-5.2 13.1-5.3 18.2-.3z'></path><rect x='0' y='0' width='512' height='512' fill='rgba(0, 0, 0, 0)'></rect></svg></a>";
	}

}

add_theme_support('woocommerce');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}


function finch_product_switch_loop_title(){
    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    add_action( 'woocommerce_after_shop_loop_item_title', 'finch_product_template_loop_product_title', 15 );
}
add_action( 'woocommerce_before_shop_loop_item', 'finch_product_switch_loop_title' );

function finch_product_template_loop_product_title() {
	global $product;
	$id = $product->id;
    echo '<h4 class="product-title">' . get_the_title() . '</h4>';
	$terms = get_the_terms( $id, 'brand' ); 
  if($terms):
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term, 'brand' );
                        if( is_wp_error( $term_link ) )
                        continue;
                    echo '<h4 class="pr-brand">' . $term->name . '</h4>';
                    }
  endif;
}


/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
	global $product;
	  
	  $args['posts_per_page'] = 5;
	  return $args;
  }
  add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
	function jk_related_products_args( $args ) {
	  $args['posts_per_page'] = 5; // 4 related products
	  $args['columns'] = 5; // arranged in 2 columns
	  return $args;
  }

	add_action( 'wp_ajax_load_more_posts', 'load_more_posts' );
    add_action( 'wp_ajax_nopriv_load_more_posts', 'load_more_posts' );
    function load_more_posts(){
			$offset = isset($_REQUEST["offset"]) ? intval($_REQUEST["offset"]) : 0;    
            $terms = get_terms( 'brand', array(
                'hide_empty' => 1,
				'offset' => $offset,
				'number' => 5
            ) );
            foreach ( $terms as $term ) {
            $args = array(
                'post_type' => 'product',
                'product_cat' => 'watches',
                'brand' => $term->slug,
                'posts_per_page' => 1
            );
            $query = new WP_Query( $args );         
            if ( $query->have_posts() ) {              
            while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="col-md-2 text-center brands-item-footer">
                <a href="<?php echo get_site_url(); ?>/shop/?yith_wcan=1&product_cat=?>&brand=<?php echo $term->slug; ?>"><?php echo get_the_post_thumbnail( get_the_ID(), 'thumb' ); ?></a>
                <a href="#" class="brand-title text-center"><?= $term->name; ?></a>
            </div>
            <?php endwhile; }
            wp_reset_postdata(); }
    }

	/* Variation Name in cart */
	
	add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );


	add_filter( 'woocommerce_add_to_cart_fragments', function($fragments) {

		ob_start();
		?>
	
		<div class="cart-contents">
			<?php echo WC()->cart->get_cart_contents_count(); ?>
		</div>
	
		<?php $fragments['div.cart-contents'] = ob_get_clean();
	
		return $fragments;
	
	} );
	
	add_filter( 'woocommerce_add_to_cart_fragments', function($fragments) {
	
		ob_start();
		?>
	
		<div class="mini">
			<h3 class="h1 text-center mini-cart-title">Shopping Cart</h3>
			<?php woocommerce_mini_cart(); ?>
		</div>
	
		<?php $fragments['div.mini'] = ob_get_clean();
	
		return $fragments;
	
	} );


	function my_wp_nav_menu_objects( $items, $args ) {
		// loop
		foreach( $items as &$item ) {
			// vars
			$image = get_field("menu_image", $item);
			// append icon
			if( $image ) {
				$item->title = "<span class='menu-image' style='background-image: url(". $image["sizes"]["medium_large"] .");'><span class='overlay-mask-4 absolute'></span></span>" .  $item->title;
			}
		}
		// return
		return $items;
	}
	add_filter("wp_nav_menu_objects", "my_wp_nav_menu_objects", 10, 2);


	add_action( 'woocommerce_before_single_product', 'move_variations_single_price', 1 );
function move_variations_single_price(){
  global $product, $post;
  if ( $product->is_type( 'variable' ) ) {
    add_action( 'woocommerce_single_product_summary', 'replace_variation_single_price', 10 );
  }
}

function replace_variation_single_price() {
  ?>
    <style>
      .woocommerce-variation-price {
        display: none;
      }
    </style>
    <script>
      jQuery(document).ready(function($) {
        var priceselector = '.product p.price';
        var originalprice = $(priceselector).html();

        $( document ).on('show_variation', function() {
          $(priceselector).html($('.single_variation .woocommerce-variation-price').html());
        });
        $( document ).on('hide_variation', function() {
          $(priceselector).html(originalprice);
        });
      });
    </script>
  <?php
}


add_theme_support( 'woocommerce', array(
	'gallery_thumbnail_image_width' => 100,
	'single_image_width' => 500,
	) );


	



// Hide Add to cart button

// Get the acf field for brand

function is_store_closed( $product_id, $taxonomy = 'brand' ) {
    $terms = wp_get_post_terms( $product_id, $taxonomy );

    if ( ! empty($terms) ) {
        $term  = reset($terms);
        
        if( is_a($term, 'WP_Term') ) {
            $value = (array) get_field( 'close_store', 'brand_' . $term->term_id );
            return reset($value) == 'Close' ? true : false;
        }

    }
    return false;
}

// Replace add to cart in Shop page
add_filter( 'woocommerce_loop_add_to_cart_link', 'replace_loop_add_to_cart_button', 10, 2 );
function replace_loop_add_to_cart_button( $button, $product  ) {
    // Not for variable products, when store is closed
    if( ! $product->is_type( 'variable' ) && is_store_closed( $product->get_id() ) ) {
        return sprintf( '<a class="button product_type_simple add_to_cart_button" href="%s">%s</a>', $product->get_permalink(), __( "Read More", "woocommerce" ) );
    }
    return $button;
}

// Replacing the single product button add to cart by a custom button when store is closed
add_action( 'woocommerce_single_product_summary', 'replace_single_add_to_cart_button', 1 );
function replace_single_add_to_cart_button() {
    global $product;

    if( is_store_closed( $product->get_id() ) ) {

        // For variable product
        if( $product->is_type( 'variable' ) ) {
            remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
            add_action( 'woocommerce_single_variation', 'custom_product_button', 20 );
        }
        // For all other product types
        else {
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
            add_action( 'woocommerce_single_product_summary', 'custom_product_button', 30 );
        }
    }
}

function custom_product_button(){
    // Display Form
    echo do_shortcode('[formidable id=4]');
}

add_filter( 'woocommerce_add_to_cart_fragments', function($fragments) {

  ob_start();
  ?>

  <p class="cart-contents">
      <?php echo WC()->cart->get_cart_contents_count(); ?>
  </p>

  <?php $fragments['p.cart-contents'] = ob_get_clean();

  return $fragments;

} );


add_filter( 'woocommerce_checkout_fields', 'bbloomer_checkout_phone_us_format' );
add_filter( 'woocommerce_billing_fields', 'bbloomer_checkout_phone_us_format' );
   
function bbloomer_checkout_phone_us_format( $fields ) {
    $fields['billing']['billing_phone']['placeholder'] = '123-456-7890';
    $fields['billing']['billing_phone']['maxlength'] = 12; // 123-456-7890 is 12 chars long
    return $fields;
}

add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_single())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}

?>

