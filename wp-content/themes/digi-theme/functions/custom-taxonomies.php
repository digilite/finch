<?php
// ADD CUSTOM TAXONOMY IF NEEDED
/*
function custom_taxonomy_type() {
	$labels = [
		"name"              => _x("Event Type", "taxonomy general name"),
		"singular_name"     => _x("Event Type", "taxonomy singular name"),
		"search_items"      => __("Search Event Types"),
		"all_items"         => __("All Event Types"),
		"parent_item"       => __("Parent Event Type"),
		"parent_item_colon" => __("Parent Event Type:"),
		"edit_item"         => __("Edit Event Type"),
		"update_item"       => __("Update Event Type"),
		"add_new_item"      => __("Add New Event Type"),
		"new_item_name"     => __("New Event Type"),
		"menu_name"         => __("Event Type"),
	];

	$args = [
		"hierarchical"      => true,
		"labels"            => $labels,
		"show_ui"           => true,
		"show_admin_column" => true,
		"query_var"         => true,
		"rewrite"           => ["slug" => "event-type"],
	];

	register_taxonomy("event-type", ["conjunctures"], $args);
}
add_action("init", "custom_taxonomy_type", 0);*/



// Register Custom Taxonomy
function ess_custom_taxonomy_Item()  {

	$labels = array(
		'name'                       => 'Brands',
		'singular_name'              => 'Brand',
		'menu_name'                  => 'Brands',
		'all_items'                  => 'All Brands',
		'parent_item'                => 'Parent Brand',
		'parent_item_colon'          => 'Parent Brand:',
		'new_item_name'              => 'New Brand Name',
		'add_new_item'               => 'Add New Brand',
		'edit_item'                  => 'Edit Brand',
		'update_item'                => 'Update Brand',
		'separate_items_with_commas' => 'Separate Brand with commas',
		'search_items'               => 'Search Brands',
		'add_or_remove_items'        => 'Add or remove Brands',
		'choose_from_most_used'      => 'Choose from the most used Brands',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array( 'slug' => 'brand', 'with_front' => false )
	);
	register_taxonomy( 'brand', 'product', $args );
	
	}
	
	add_action( 'init', 'ess_custom_taxonomy_item', 0 );


	function col_custom_taxonomy_Item()  {

		$labels = array(
			'name'                       => 'Collections',
			'singular_name'              => 'Collection ',
			'menu_name'                  => 'Collections',
			'all_items'                  => 'All Collections',
			'parent_item'                => 'Parent Collection',
			'parent_item_colon'          => 'Parent Collection:',
			'new_item_name'              => 'New Collection Name',
			'add_new_item'               => 'Add New Collection',
			'edit_item'                  => 'Edit Collection',
			'update_item'                => 'Update Collection',
			'separate_items_with_commas' => 'Separate Collection with commas',
			'search_items'               => 'Search Collections',
			'add_or_remove_items'        => 'Add or remove Collections',
			'choose_from_most_used'      => 'Choose from the most used Collections',
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'collection', 'product', $args );
		
		}
		
		add_action( 'init', 'col_custom_taxonomy_item', 0 );


		function gender_custom_taxonomy_Item()  {

			$labels = array(
				'name'                       => 'Gender',
				'singular_name'              => 'Gender ',
				'menu_name'                  => 'Gender',
				'all_items'                  => 'All Genders',
				'parent_item'                => 'Parent Gender',
				'parent_item_colon'          => 'Parent Gender:',
				'new_item_name'              => 'New Gender Name',
				'add_new_item'               => 'Add New Gender',
				'edit_item'                  => 'Edit Gender',
				'update_item'                => 'Update Gender',
				'separate_items_with_commas' => 'Separate Gender with commas',
				'search_items'               => 'Search Genders',
				'add_or_remove_items'        => 'Add or remove Genders',
				'choose_from_most_used'      => 'Choose from the most used Genders',
			);
			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => true,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => true,
				'show_tagcloud'              => true,
			);
			register_taxonomy( 'gender', 'product', $args );
			
			}
			
			add_action( 'init', 'gender_custom_taxonomy_item', 0 );