   <section class="hero">
   <?php $slide_image = get_field('hero_slider'); ?>
                <?php if( have_rows('hero_slider') ): ?>
                <div class="hero-main-slider owl-carousel owl-theme" id="owl-demo">
                    <?php while( have_rows('hero_slider') ) : the_row();
                        $slide_item = get_sub_field('hero_slide_item');
                        $size = 'finch-hero'; // (thumbnail, medium, large, full or custom size)
                        $slide_item_url = get_sub_field('slide_item_url'); ?>
                        <div class="item">
                            <a href="<?= $slide_item_url; ?>">
                               <?php echo wp_get_attachment_image( $slide_item, $size ); ?>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php else: ?>
		            <?php echo get_the_post_thumbnail(); ?>
                <?php endif; ?>
    <div class="logo-carousel">
        <div class="container">
            <div class="partners owl-carousel owl-theme">
            <?php  $terms = get_terms( 'brand', array(
                    'hide_empty' => 1
                ) ); 
                foreach ( $terms as $term ) { 
                    $current_cat_id = $term->term_id; 
                    $brand_image = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    if($brand_image):
                    $brand_thumb = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    else:
                    $brand_thumb = get_field('brands_default_image', 'option');
                    endif;
                    ?>        
                    <a href="<?php echo get_site_url(); ?>/brand/<?php echo $term->slug; ?>" class="owl-item">
                        <div class="d-flex partner-l justify-content-center align-items-center">
                            <img class="partner-logo" src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>" />
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>