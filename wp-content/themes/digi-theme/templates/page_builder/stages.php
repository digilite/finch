<section class="stages ">
    <div class="container">
        <div class="relative-parent">
            <div class="stages-line"></div>
            <?php $g_title = get_sub_field("stages_g_title"); ?>
            <h3 class="h1 text-center stage-block-title"><?= $g_title; ?></h3>
            <div class="flex-container stages-blocks">
            <?php

    if( have_rows('stages_items') ):
        while( have_rows('stages_items') ) : the_row();
            $image = get_sub_field('stages_image');
            $icon = get_sub_field('stages_icon');
            $title= get_sub_field('stages_title');
            $description = get_sub_field('stages_description');?>
            <div class="col-md-4 text-center">
                <?php if( !empty($image) ): ?>
                    <img class="stages_image" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                <?php endif; ?>
                <img class="stages-icon" src="<?= $icon; ?>" alt="icon">
                <h4 class="stages-item-title"><?= $title; ?></h4>
                <p><?= $description; ?></p>
            </div>
        <?php endwhile;

    // No value.
    else :
        // Do something...
    endif; ?>
            </div>
        </div>
    </div>
</section>