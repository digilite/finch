<?php
$sh_title = get_sub_field('shop_by_title');
$sh_description = get_sub_field('shop_by_description');
$sh_button = get_sub_field('shop_by_button');
$sh_button_url = get_sub_field('shop_by_button_url');
$what_to_show = get_sub_field('choose_taxanomy_to_show');
$choose_cat = get_sub_field('choose_category');
$choose_brand = get_sub_field('choose_brand');
$term_cat = get_term( $choose_cat, 'product_cat' );
$slug_cat = $term_cat->slug;
$term_brand = get_term( $choose_brand, 'product_cat' );
$slug_brand = $term_brand->slug;
if(($what_to_show == "product_cat") || ($what_to_show == "subcategory")) {
    $bturl = get_site_url() .'shop/products/product_cat/'. $choose_cat;
} elseif($what_to_show == "brand") {
    $bturl = get_site_url() .'shop/products/product_cat/'. $choose_cat  .'/brand/'. $choose_brand;
}
?>
<section class="shop-by section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="shop-by-title h1"><?= $sh_title; ?></h3>
                <p class="shop-by-desc"><?= $sh_description; ?></p>
                <a class="button_white shop-by-button" href="<?= $sh_button_url; ?>"><?= $sh_button; ?></a>
            </div>
            <div class="col-md-8">
                <?php if($what_to_show == "product_cat"): ?>
                <div class="flex-container justify-content-between show-categories owl-carousel owl-theme">
                    <?php $taxonomies = get_terms(array(
                            'taxonomy' => 'product_cat',
                            'hide_empty' => false
                        ) );
                        if (!empty($taxonomies)) :
                            foreach($taxonomies as $category){
                                if($category->parent == 0){ ?>
                                    <div class="single-cat-item item">
                                    <?php $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                                            $size = 'cat-thumb';
                                            $image = wp_get_attachment_url( $thumbnail_id ); ?>
                                        <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $category->slug; ?>"><img src="<?= $image; ?>" alt="category"></a>
                                        <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $category->slug; ?>" class="cat_name text-center"><?= $category->name; ?></a>
                                    </div>
                                <?php }
                            }
                        endif; ?>
                </div>
                <?php elseif($what_to_show == "brand"): ?>
                <div class="show-categories owl-carousel owl-theme brands-block child-collection">
                    <?php
                    $terms = get_terms( 'brand', array(
                        'hide_empty' => 1
                    ) );
                    foreach ( $terms as $term ) {
                    $args = array(
                        'post_type' => 'product',
                        'product_cat' => $slug_cat,
                        'brand' => $term->slug,
                        'posts_per_page' => 1
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="item">
                    <?php $current_cat_id = $term->term_id;
                    $brand_image = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    if($brand_image):
                    $brand_thumb = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    else:
                    $brand_thumb = get_field('brands_default_image', 'option');
                    endif;
                    ?>
                        <?php $term_s = get_term( $choose_cat ); ?>
                            <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $term_s->slug; ?>/brand/<?php echo $term->slug; ?>">
                                <div class="d-flex partner-l justify-content-center align-items-center">
                                    <img class="partner-logo" src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>" />
                                </div>
                            </a>
                    </div>
                    <?php endwhile; }
                    wp_reset_postdata(); } ?>
                </div>
                <?php elseif($what_to_show == "subcategory") : ?>
                <div class="show-categories child-of-parent owl-carousel owl-theme child-subategory">
                <?php
                    $term_cat = get_term_by('slug', $slug_cat, 'product_cat');
                    $termID = $term_cat->term_id;
                    $taxonomyName = "product_cat";
                    $termchildren = get_term_children( $termID, $taxonomyName );
                    foreach ($termchildren as $child) {
                        $term = get_term_by( 'id', $child, $taxonomyName );
                        $thumb_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                        $term_img = wp_get_attachment_url(  $thumb_id ); ?>
                        <div class="item">
                            <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $term->slug; ?>"><img src="<?= $term_img; ?>" alt="<?= $term->name; ?>"></a>
                            <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $term->slug; ?>" class="brand-title text-center"><?= $term->name; ?></a>
                        </div>
                    <?php }
                ?>
                </div>
                <?php elseif($what_to_show == 'collection') : ?>
                <div class="show-categories owl-carousel owl-theme collections-block child-collection">
                    <?php
                    $terms = get_terms( 'collection', array(
                        'hide_empty' => 1
                    ) );
                    foreach ( $terms as $term ) {
                    $args = array(
                        'post_type' => 'product',
                        'product_cat' => $slug_cat,
                        'brand' => $slug_brand,
                        'collection' => $term->slug,
                        'posts_per_page' => 1
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="item">
                        <?php $term_s = get_term( $choose_cat ); ?>
                        <?php $term_b = get_term( $choose_brand ); ?>
                        <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $choose_cat->slug; ?>/brand/<?php echo $term_b->slug; ?>/collection/<?php echo $term->slug; ?>"><?php echo get_the_post_thumbnail( get_the_ID(), 'thumb' ); ?></a>
                        <a href="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>products/product_cat/<?php echo $choose_cat->slug; ?>/brand/<?php echo $term_b->slug; ?>/collection/<?php echo $term->slug; ?>" class="brand-title text-center"><?= $term->name; ?></a>
                    </div>
                    <?php endwhile; }
                    wp_reset_postdata(); } ?>
                </div>
                <?php else :
                    $featured_posts = get_sub_field('sh_choose_products');
                    if( $featured_posts ): ?>
                    <div class="child-of-parent owl-carousel owl-theme collections-block child-collection">

                            <?php foreach( $featured_posts as $post ): ?>
                            <div class="item prod-item">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="sh_thumb">
                                        <?php the_post_thumbnail('woocommerce_single', array("class" => "img-fluid")); ?>
                                    </div>
                                    <h4 class="product-title text-center"><?php the_title(); ?></h4>
                                    <?php
                                    $post_terms_array=get_the_terms($product, 'brand');
                                    $_product = wc_get_product( $post->ID ); ?>
                                    <h4 class="pr-brand text-center"><?php echo $post_terms_array[0]->name; ?></h4>
                                    <span class="price text-center"><span class="woocommerce-Price-amount amount text-center">$<?php echo $_product->get_price(); ?></span>
                                </a>
                                <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'" show_price="false"]'); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata();
                    endif;
                endif; ?>
            </div>
            <a class="button_white mobile-shop-by-button" href="<?= $sh_button_url; ?>"><?= $sh_button; ?></a>
        </div>
    </div>
</section>


