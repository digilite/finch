<?php
    $desc = get_sub_field("contact_description");
    $loc_title = get_sub_field("contact_title");
    $loc_address = get_sub_field("contact_address");
    $loc_phone = get_sub_field("contact_phone");
    $loc_email = get_sub_field("contact_email");
    $hours_title = get_sub_field("hours_title");
?>
<section class="contact-us">
    <div class="container">
        <h1 class="h1 text-center"><?php the_title(); ?></h1>
        <p class="contact-desc"><?= $desc; ?></p>
        <div class="row justify-content-between contact-info-main">
            <div class="col-md-5">
                <h3 class="h1"><?= $loc_title; ?></h3>
                <div class="address flex-container justify-content-between">
                    <img src="<?php bloginfo('template_directory'); ?>/img/location.svg" alt="address">
                    <p class="address_info col-md-10"><?= $loc_address; ?>
                </div>
                <div class="phone flex-container justify-content-between">
                    <img src="<?php bloginfo('template_directory'); ?>/img/phone_2.svg" alt="phone">
                    <?php
                      $clear_phone = str_replace(' ', '', $loc_phone);
                      $part_phone = str_replace(')', '', $clear_phone);
                      $part2_phone = str_replace('(', '', $part_phone);
                      $final_phone = str_replace('-', '', $part2_phone);
                    ?>
                    <a href="tel:<?= $final_phone; ?>" class="phone_info col-md-10"><?= $loc_phone; ?></a>
                </div>
                <div class="email flex-container justify-content-between">
                  <img src="<?php bloginfo('template_directory'); ?>/img/email.svg" alt="email">
                  <a href="mailto:<?= $loc_email; ?>" class="address_info col-md-10"><?= $loc_email; ?></a>
                </div>
            </div>
            <div class="col-md-5">
                <h3 class="h1"><?= $hours_title; ?></h3>
                <div>
                    <?php if( have_rows('add_hours') ):
                    while( have_rows('add_hours') ) : the_row();
                    $day = get_sub_field('work_day');
                    $hours = get_sub_field('work_hours'); ?>
                    <div class="work-hours-block flex-container justify-content-between">
                        <p class="work-day col-md-6"><?= $day; ?></p>
                        <p class="work-hours col-md-6"><?= $hours; ?></p>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php else :
        endif; ?>
            </div>
        </div>
    </div>
</section>
