<?php 
$pr_image = get_sub_field("pr_info_background_image");
$pr_title = get_sub_field("pr_info_title");
$pr_subtitle = get_sub_field("pr_info_subtitle");
$pr_description = get_sub_field("pr_info_description");
$pr_button = get_sub_field("pr_info_button");
$pr_button_url = get_sub_field("pr_info_button_url");
?>
<section class="product-info">
    <div class="container">
        <div class="row justify-content-center">
            <div class="info-block" style="background: url(<?= $pr_image; ?>) center center / cover no-repeat;">
                <div class="col-md-7 pr-info text-center">
                    <h3 class="h1"><?= $pr_title; ?></h3>
                    <p class="subtitle"><?= $pr_subtitle; ?></p>
                    <p class="description"><?= $pr_description; ?></p>
                    <a class="dark_button" href="<?= $pr_button_url; ?>"><?= $pr_button; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>