<?php 
$cf_title = get_sub_field("cf_title");
$cf_description = get_sub_field("cf_description");
$cf_form_title = get_sub_field("cf_form_title");
?>
<section class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3 class="h1"><?= $cf_title; ?></h3>
                <p class="description"><?= $cf_description; ?></p>
            </div>
            <div class="col-md-7">
                <p class="form-title"><?= $cf_form_title; ?></p>
                <?php echo do_shortcode('[formidable id=1]'); ?>
            </div>
        </div>
    </div>
</section>