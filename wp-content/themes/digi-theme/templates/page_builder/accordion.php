<section class="accordion">
<div class="container">
<h1 class="h1 text-center ne-title"><?= get_the_title(); ?></h1>
<div class="b-faq">
<?php $firs_image = get_sub_field("acc_first_image"); ?>
<?php if( !empty( $firs_image) ): ?>
     <img class="acc_first_image" src="<?php echo esc_url($firs_image['url']); ?>" alt="<?php echo esc_attr($firs_image['alt']); ?>" />
<?php endif; ?>
<?php if( have_rows('accordion_items') ):
    while( have_rows('accordion_items') ) : the_row();
        $acc_title = get_sub_field('acc_item_title');
        $acc_description = get_sub_field('acc_description');
        $service_icon = get_sub_field('sr_icon'); ?>
                
                
				<div class="faq__item">
                        <div class="faq__title js-faq-title">
                            <div class="faq__spoiler js-faq-rotate"><span class="faq__symbol "><img src="<?php bloginfo('template_directory'); ?>/img/arr.svg" alt="arr"></span></div> 
                            <span class="faq__text h3"><?= $acc_title; ?></span>
                        </div>
                        <?php if($acc_description) : ?>
                        <div class="faq__content js-faq-content">
                            <?= $acc_description; ?>
                        </div>
                        <?php endif; ?>
                        <?php if( have_rows('acc_blocks') ):?>
                        <div class="faq__content js-faq-content">
                            <div class="flex-container justify-content-between">
                                <?php while( have_rows('acc_blocks') ) : the_row();
                                $acc_r_image = get_sub_field('acc_r_image');
                                $acc_r_title = get_sub_field('acc_r_title');
                                $acc_r_description = get_sub_field('acc_r_description'); ?>
                                <div class="acc-r-item">
                                    <?php if( !empty( $acc_r_image ) ): ?>
                                    <img class="acc_r_image" src="<?php echo esc_url($acc_r_image['url']); ?>" alt="<?php echo esc_attr($acc_r_image['alt']); ?>" />
                                    <?php endif; ?>
                                    <h4 class="acc-r-title"><?= $acc_r_title; ?></h4>
                                    <p><?= $acc_r_description; ?></p>
                                </div>   
                                <?php endwhile; ?>
                            </div>
					    </div>
                        <?php else :
                        endif; ?>
				</div>
        <?php endwhile;
        else :
        endif; ?>
    </div>
</section>