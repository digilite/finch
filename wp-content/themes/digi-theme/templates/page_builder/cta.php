<?php 
    $cta_title = get_sub_field("cta_title");
    $cta_description = get_sub_field("cta_description");
    $cta_button = get_sub_field("cta_button");
    $cta_button_url = get_sub_field("cta_button_url");
    $cta_image = get_sub_field("cta_background_image");
?>
<section class="cta" style="background: url(<?= $cta_image; ?>) center center / cover no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="cta-desktop-info">
                    <h3 class="h1 cta-title"><?= $cta_title; ?></h3>
                    <p><?= $cta_description; ?></p>
                    <a href="<?= $cta_button_url; ?>" class="button_white"><?= $cta_button; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="cta-mobile-info">
    <div class="container">
        <div class="col-md-12">
            <div class="cta-mb">
                <h3 class="h1 cta-title"><?= $cta_title; ?></h3>
                <p><?= $cta_description; ?></p>
                <a href="<?= $cta_button_url; ?>" class="button_white"><?= $cta_button; ?></a>
            </div>
        </div>
    </div>
</div>