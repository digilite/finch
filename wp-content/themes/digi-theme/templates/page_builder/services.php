<?php
  $sr_title = get_sub_field("sr_title");
  $sr_description = get_sub_field("sr_description");
?>
<section class="services">
    <div class="container">
        <div class="row services-container">
            <div class="col-md-4 services-title-block">
                <h3 class="h1"><?= $sr_title; ?></h3>
                <p class="description"><?= $sr_description; ?></p>
            </div>
            <div class="col-md-8 flex-container justify-content-between services-list">
              <?php
                if( have_rows('services_list') ):
                    while( have_rows('services_list') ) : the_row();
                        $service_name = get_sub_field('service_name');
                        $service_icon = get_sub_field('sr_icon');
              ?>
              <div class="service_item text-center">
              <?php if( !empty( $service_icon ) ): ?>
                  <img class="owl-item" src="<?php echo esc_url($service_icon['url']); ?>" alt="<?php echo esc_attr($service_icon['alt']); ?>" />
              <?php endif; ?>
              <p class="service_name"><?= $service_name; ?></p>
            </div>
        <?php endwhile;
        else :
        endif; ?>
            </div>
        </div>
    </div>
</section>
