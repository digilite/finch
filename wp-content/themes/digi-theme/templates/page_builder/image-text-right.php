<?php
$block_style = get_sub_field("imgtr_choose_block_style");
if($block_style == 'image-text-right') {
    $col1 = "col-md-6";
    $col2 = "col-md-6";
    $pr = "pr-0";
    $pl = "pl-0";
} else if ($block_style == 'image-text-left') {
    $col1 = "col-md-7";
    $col2 = "col-md-5";
    $pr = "pr-0";
    $pl = "pl-0";
} else if ($block_style == 'text-small-image') {
    $col1 = "col-md-8";
    $col2 = "col-md-4";
    $pr = "";
    $pl = "";
}
$imgtr_title = get_sub_field('imgtr_title');
$imgtr_description = get_sub_field('imgtr_description');
$imgtr_button = get_sub_field('imgtr_button');
$imgtr_button_url = get_sub_field('imgtr_button_url');
$imgtr_image = get_sub_field('imgtr_image');
$imgtr_subtitle = get_sub_field("imgtr_subtitle");
 ?>
<section class="<?= $block_style; ?>">
    <div class="container">
        <h3 class="h1 tablet-title text-center"><?= $imgtr_title; ?></h3>
        <div class="row">
            <div class="<?= $col1; ?> <?= $pr; ?>">
                <div class="imgtr-left">
                    <?php if($block_style == 'image-text-left') : ?>
                        <h3 class="h1"><?= $imgtr_title; ?></h3>
                        <p><?= $imgtr_description; ?></p>
                        <a href="<?= $imgtr_button_url; ?>" class="more-button"><?= $imgtr_button; ?></a>
                        <?php else : ?>
                        <div class="img-item" style="background-image: url(<?php echo esc_url($imgtr_image['url']); ?>);">
                        </div>
                    <?php endif; ?>
                        </div>
            </div>
            <div class="<?= $col2; ?> <?= $pl; ?>">
                <div class="imgtr-right">
                    <?php if($block_style == 'image-text-left') : ?>
                        <?php if( !empty( $imgtr_image) ): ?>
                            <img class="imgtr-image" src="<?php echo esc_url($imgtr_image['url']); ?>" alt="<?php echo esc_attr($imgtr_image['alt']); ?>" />
                        <?php endif;
                        else : ?>
                        <h3 class="h1"><?= $imgtr_title; ?></h3>
                        <?php if($imgtr_subtitle) : ?><p class="subtitle"><?= $imgtr_subtitle; ?></p><?php endif; ?>
                        <p class="description"><?= $imgtr_description; ?></p>
                        <a href="<?= $imgtr_button_url; ?>" class="more-button"><?= $imgtr_button; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>