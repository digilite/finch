<?php
$tx_title = get_sub_field("tx_title");
$tx_subtitle = get_sub_field("tx_subtitle");
$tx_description = get_sub_field("tx_description");
?>
<section class="text-info-block">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-10">
                <h1 class="h1"><?= $tx_title; ?></h1>
                <p class="subtitle"><?= $tx_subtitle; ?></p>
                <?= $tx_description; ?>
            </div>
        </div>
    </div>
</section>
