<form action="<?php bloginfo("url"); ?>" method="get" id="search" class="form-inline transition flex-container justify-content-between">
	<button class="transition search-submit" type="submit"><img src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="search"></button>
	<input class="search-bar transition" type="search" value="<?php (isset($_GET["s"]) ? _e($_GET["s"]) : ""); ?>" type="search" name="s" autocomplete="off" placeholder="SEARCH...">
</form>
