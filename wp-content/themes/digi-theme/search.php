<?php get_header();
$hero_image = get_field('shop_page_hero', 'option'); ?>

<section class="hero">
   <?php if( !empty($hero_image) ): ?>
        <img style="width: 100%;" class="imgtr-image" src="<?php echo esc_url($hero_image['url']); ?>" alt="<?php echo esc_attr($hero_image['alt']); ?>" />
    <?php endif; ?>
    <div class="logo-carousel">
        <div class="container">
            <div class="partners owl-carousel owl-theme">
            <?php  $terms = get_terms( 'brand', array(
                    'hide_empty' => 1
                ) ); 
                foreach ( $terms as $term ) { 
                    $current_cat_id = $term->term_id; 
                    $brand_image = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    if($brand_image):
                    $brand_thumb = get_field('brand_thumbnail', 'brand_'.$current_cat_id);
                    else:
                    $brand_thumb = get_field('brands_default_image', 'option');
                    endif;
                    ?>        
                    <a href="<?php echo get_site_url(); ?>/brand/<?php echo $term->slug; ?>" class="owl-item">
                        <div class="d-flex partner-l justify-content-center align-items-center">
                            <img class="partner-logo" src="<?php echo esc_url($brand_thumb['url']); ?>" alt="<?php echo esc_attr($brand_thumb['alt']); ?>" />
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<div class="container ">
	<h2 class="h1 search-page-title">Search Results</h2>
	<div class="row">
		<?php
		if (have_posts()) :
			while (have_posts()) :
				the_post(); ?>
				<div class="col-md-4 search-item">
					<?php the_post_thumbnail('medium'); ?>
					<h4 class="product-title text-center"><?php the_title(); ?></h4>
					<?php $product = wc_get_product( $post_id ); ?>
					<p><?php the_content(); ?></p>
					<a href="<?= the_permalink(); ?>" class="button product_type_simple add_to_cart_button ajax_add_to_cart search-red-more">Read More</a>
				</div>
				<?php
			endwhile;
		endif; ?>
	</div>
</div>

<?php get_footer(); ?>